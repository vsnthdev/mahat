<img src="https://raw.githubusercontent.com/vasanthdeveloper/mahat/designs/renders/banner.png" alt="mahat"><br>
<p align="center"><strong>( महत् ) — The ✨ content delivery API ⚡️ for Vasanth Developer.</strong></p>

## ⚡️ Routes
| Method | Endpoint | Description |
|-|-|-|
| `GET` | `/` | Responds with my personal information. |
| `GET` | `/projects` | List of projects I'm working on, organizations I've created. |
| `GET` | `/tweets` | Curated feed of my Twitter tweets and threads. |
| `GET` | `/videos` | Feed of my latest YouTube videos. |
